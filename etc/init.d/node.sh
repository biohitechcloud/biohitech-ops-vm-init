#!/bin/bash
#
# Startup script for a node project
#
# chkconfig: - 84 16
# description: node project

# Source function library.
[ -f "/etc/rc.d/init.d/functions" ] && . /etc/rc.d/init.d/functions

# App Name
APP_NAME=$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")

# Override Settings
[ -f /etc/profile.d/node.sh ] && source /etc/profile.d/node.sh
[ -f /etc/profile.d/${APP_NAME}.sh ] && source /etc/profile.d/${APP_NAME}.sh

# Default Forever Settings
[ -z "$FOREVER_EXEC" ] && FOREVER_EXEC=/usr/local/bin/forever
[ -z "$FOREVER_MIN_UPTIME" ] && FOREVER_MIN_UPTIME=5000
[ -z "$FOREVER_SPIN_SLEEP_TIME" ] && FOREVER_SPIN_SLEEP_TIME=5000

# Default App Settings
[ -z "$APP_HOME" ] && APP_HOME=/opt/bht/${APP_NAME}
[ -z "$APP_LOCK_HOME" ] && APP_LOCK_HOME="/var/lock/subsys"
[ -z "$APP_LOCK" ] && APP_LOCK="$APP_OCK_HOME/$APP_NAME"
[ -z "$APP_LOG_HOME" ] && APP_LOG_HOME="/opt/bht/logs"
[ -z "$APP_LOG" ] && APP_LOG="$APP_LOG_HOME/$APP_NAME.log"
[ -z "$APP_MODULES" ] && APP_MODULES=biohitech
[ -z "$APP_PROFILE" ] && APP_PROFILE=production
[ -z "$APP_SCRIPT" ] && APP_SCRIPT="${APP_HOME}/app.js"
[ -z "$APP_USER" ] && APP_USER=bht

# Default Node Settings
[ -z "$NODE_EXEC" ] && NODE_EXEC=/usr/local/bin/node
[ -z "$NODE_OPTS" ] && NODE_OPTS="$EXTRA_NODE_OPTS"

if [ ! -x "$FOREVER_EXEC" ]; then
    echo "ERROR: forever does not exists at $FOREVER_EXEC. You need to sudo npm install -g forever."
    exit 1
fi

NODE_ENV=$APP_PROFILE
NODE_PROFILES=$APP_MODULES
NODE_OPTS=`echo $NODE_OPTS | xargs`

export APP_HOME

RETVAL=0

exec_pattern="node.*$APP_NAME"

pid_of_forever() {
    ps -ef | grep "$exec_pattern" | grep forever | grep -v grep | awk '{print $2}'
}

pid_of_app() {
    ps -ef | grep "$exec_pattern" | grep -v forever | grep -v grep | awk '{print $2}'
}

start() {
    pidforever=`pid_of_forever`
    pidapp=`pid_of_app`

    if [ "$pidforever" != "" ]; then
        echo "ERROR: Forever process for $APP_NAME is already running with pid: $pidforever"
        exit 1
    fi

    if [ "$pidapp" != "" ]; then
        echo "ERROR: Application process for $APP_NAME is already running with pid: $pidapp"
        exit 1
    fi

    echo -n $"Starting $APP_NAME: "

    cd "$APP_HOME"    
    su - ${APP_USER} -c "export NODE_ENV=$NODE_ENV && export NODE_PROFILES=$NODE_PROFILES && $FOREVER_EXEC start -c \"$NODE_EXEC $NODE_OPTS\" --uid \"$APP_NAME\" --minUptime $FOREVER_MIN_UPTIME --spinSleepTime $FOREVER_SPIN_SLEEP_TIME --no-colors -l $APP_LOG -a --workingDir $APP_HOME $APP_SCRIPT > /dev/null"

    cnt=10
    while [ $cnt -gt 0 ] ; do
        pidforever=`pid_of_forever`
        pidapp=`pid_of_app`

        if [ -n "$pidapp" ] && [ -n "$pidforever" ]; then
            break
        fi

        sleep 1
        ((cnt--))
    done

    if [ $cnt -gt 0 ]; then
        RETVAL=0
    else 
        RETVAL=1
    fi

    [ ${RETVAL} = 0 ] && success $"$STRING" || failure $"$STRING"
    echo
    [ ${RETVAL} = 0 ] && touch "$APP_LOCK"
}

stop() {
    echo -n "Stopping $APP_NAME: "

    pidforever=`pid_of_forever`
    pidapp=`pid_of_app`

    if [ -z "$pidapp" ] && [ -z "$pidforever" ]; then
        RETVAL=0
    else
        echo "$pidforever" | xargs kill > /dev/null 2>&1
        echo "$pidapp" | xargs kill > /dev/null 2>&1

        cnt=10
        while [ $cnt -gt 0 ] ; do
            pidforever=`pid_of_forever`
            pidapp=`pid_of_app`

            if [ -z "$pidapp" ] && [ -z "$pidforever" ]; then
                break
            fi

            sleep 1
            ((cnt--))
        done

        if [ $cnt -gt 0 ]; then
            RETVAL=0
        else 
            RETVAL=1
        fi
    fi
    
    [ $RETVAL = 0 ] && rm -f "$APP_LOCK"
    [ $RETVAL = 0 ] && success $"$STRING" || failure $"$STRING"
    echo
}

status() {
    pidapp=`pid_of_app`
    pidforever=`pid_of_forever`

    if [ -z "$pidapp" ]; then
        echo  "$APP_NAME is stopped."
        return 1
    fi

    if [ -z "$pidforever" ]; then
        echo  "forever process for $APP_NAME is stopped."
        return 2
    fi

    if [ -z "$pidapp" ] && [ -f "$APP_LOCK" ]; then
        echo "${base} dead lock file $APP_LOCK exists."
        return 3
    fi

    echo "$APP_NAME (pid $pidapp) is running."
    return 0
}

# See how we were called.
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    restart)
        stop
        start
        ;;
    *)
    echo $"Usage: $0 {start|stop|restart|status}"
    exit 1  
esac

exit $RETVAL
#!/bin/sh

if [ "$USER" != 'root' ]; then 
	echo "You must be root."
	exit 1
fi

AWS_ACCOUNT=$(aws sts get-caller-identity --output text --query 'Account')
PRODUCT=
SALT_MASTER=

echo "Looking up AWS Account"

if [ "$AWS_ACCOUNT" == "989766181581" ]; then
	PRODUCT="biohitech"
	SALT_MASTER="ops1-prod.bht"
elif [ "$AWS_ACCOUNT" == "139917026365" ]; then
	PRODUCT="gme"
	SALT_MASTER="gme-ops-prod.gme"
else
	echo "Unable to AWS Account."
	exit 1	
fi

echo "AWS ACCOUNT: $AWS_ACCOUNT"
echo "PRODUCT: $PRODUCT"
echo "SALT_MASTER: $SALT_MASTER"

ROLES="$PRODUCT"

while [ -z "$NEW_HOST_NAME" ]; do
	read -p "Please Enter the Host Name For this VM: " NEW_HOST_NAME

	# if [[ "$NEW_HOST_NAME" == cloud-* ]]; then
	# 	echo ""
	# 	echo "**** ERROR: Please don't prefix with cloud-.  It's redundant."
	# 	echo ""
	# 	NEW_HOST_NAME=""
	# fi	
done

while [ -z "$NEW_HOST_NAME_CONFIRM" ]; do
	read -p "Confirm Host Name: " NEW_HOST_NAME_CONFIRM
done

if [ "$NEW_HOST_NAME" != "$NEW_HOST_NAME_CONFIRM" ]; then
	echo "Host Names do not match."
	exit 1
fi

while [ "$SES" != "y" ] && [ "$SES" != "n" ]; do
	read -p "Do you want to use Amazon Simple Email Service on this VM (y/n)? " SES
done

if [ "$SES" = "y" ]; then
	ROLES="$ROLES ses"
fi

while [ "$NEWRELIC" != "y" ] && [ "$NEWRELIC" != "n" ]; do
	read -p "Do you want to install the New Relic System Agent (y/n)? " NEWRELIC
done

if [ "$NEWRELIC" = "y" ]; then
    ROLES="$ROLES newrelic"
fi

while [ "$JAVA" != "y" ] && [ "$JAVA" != "n" ]; do
	read -p "Do you want to install java (y/n)? " JAVA
done

while [ "$CODEDEPLOY" != "y" ] && [ "$CODEDEPLOY" != "n" ]; do
	read -p "Do you want to install the AWS CodeDeploy Agent (y/n)? " CODEDEPLOY
done

while [ "$SERVICE" = "" ]; do
	read -p "Enter the name of the service that will be running on this VM (Enter 'none' to skip): " SERVICE
done

echo "Updating yum"
yum -y update

echo "Installing Telnet"
yum -y install telnet

echo "Installing sysstat"
yum -y install sysstat

echo "Installing node"
mkdir -p /opt/node
wget -P /opt/node https://static.biohitechcloud.com/vm-init/distros/node/node.tar.gz
tar zxf /opt/node/node.tar.gz -C /opt/node
rm -f /opt/node/node.tar.gz
ln -s /opt/node/node* /opt/node/latest
ln -s /opt/node/latest/bin/node /usr/local/bin/node
ln -s /opt/node/latest/bin/npm /usr/local/bin/npm

echo "Installing forever"
npm install -g forever
ln -s /opt/node/latest/bin/forever /usr/local/bin/forever

if [ "$JAVA" = "y" ]; then
	echo "Installing java JDK"
	mkdir -p /opt/java
	wget -P /opt/java https://static.biohitechcloud.com/vm-init/distros/java/jdk.tar.gz
	tar zxf /opt/java/jdk.tar.gz -C /opt/java
	rm -f /opt/java/jdk.tar.gz
	ln -s /opt/java/jdk* /opt/java/latest
	ln -s /opt/java/latest/bin/java /usr/local/bin/java
	ln -s /opt/java/latest/bin/jar /usr/local/bin/jar
fi

if [ "$SERVICE" != "none" ]; then
	echo "Setting up init script for service $SERVICE."
	SERVICE_FILE="node.sh"

	if [ "$JAVA" = "y" ]; then
		SERVICE_FILE="java.sh"
	fi

	wget -P /etc/init.d https://bitbucket.org/biohitechcloud/biohitech-ops-vm-init/raw/master/etc/init.d/$SERVICE_FILE
	mv /etc/init.d/$SERVICE_FILE /etc/init.d/$SERVICE
	chmod +x /etc/init.d/$SERVICE
	chkconfig --add $SERVICE
	chkconfig $SERVICE on
fi

echo "Setting Python version to 2.6"
update-alternatives --set python /usr/bin/python2.6

echo "Installing Salt Minion"
yum -y install https://repo.saltstack.com/yum/amazon/salt-amzn-repo-latest-2.amzn1.noarch.rpm
yum -y install salt-minion
cp /etc/salt/minion /etc/salt/minion.orig
mkdir -p /etc/salt/minion.d

echo "master: $SALT_MASTER" > /etc/salt/minion.d/bht.conf
echo "id: $NEW_HOST_NAME" >> /etc/salt/minion.d/bht.conf
echo "grains:" >> /etc/salt/minion.d/bht.conf
echo "  roles:" >> /etc/salt/minion.d/bht.conf
for role in $ROLES; do
	echo "    - $role" >> /etc/salt/minion.d/bht.conf
done
service salt-minion start

echo "Installing s3cmd"
yum -y install s3cmd

echo "Enabling SSH Password Authentication"
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
/etc/init.d/sshd restart

echo "Setting Timezone"
cd /etc; rm -f localtime; ln -s /usr/share/zoneinfo/US/Eastern localtime; cd -

echo "Displaying Proper Filesystem Sizes"
resize2fs /dev/xvda1

echo "Setting Host Name"
hostname $NEW_HOST_NAME
sed -i "s/HOSTNAME=.*/HOSTNAME=$NEW_HOST_NAME/g" /etc/sysconfig/network
sed -i -r "s/^(127\.0\.0\.1.*)$/\1 $NEW_HOST_NAME/g" /etc/hosts

if [ "$CODEDEPLOY" = "y" ]; then
	echo "Installing AWS CodeDeploy Agent"
	wget http://aws-codedeploy-us-east-1.s3.amazonaws.com/latest/install
	chmod +x ./install
	sudo ./install auto
fi

printf "Host $NEW_HOST_NAME is initialized.\n\n*** On $SALT_MASTER, please run the following:\n\nsalt-key -y -a $NEW_HOST_NAME\nsalt '$NEW_HOST_NAME' state.highstate\n\n"
history -c
